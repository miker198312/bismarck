<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServicePicture extends Model
{
	protected $table = 'services_picture';

	protected $fillable = [
        'file_name','file_extensions','file_size','description'
    ];
}