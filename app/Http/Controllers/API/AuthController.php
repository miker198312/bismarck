<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Notifications\SignupActivate;
use Illuminate\Support\Str;
use Carbon\Carbon;

class AuthController extends Controller
{
    //
     public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed',
            'last_name' => 'required|max:55',
            'maiden_name' => 'required|max:55',
            'mobile_phone_number' => 'required|max:55',            
            'county_id' => 'required',
            'street' => 'required|max:55',
            'external_number' => 'required|max:55'
        ]);


        $validatedData['password'] = bcrypt($request->password);
        $validatedData['activation_token'] = Str::random(40);

        $user = User::create($validatedData);

        $user->notify(new SignupActivate($user));

        $accessToken = $user->createToken('authToken')->accessToken;

        return response([ 'access_token' => $accessToken]);
    }

    public function login(Request $request)
    {        
        //return $request;
        $request->validate([
            'email'       => 'required|string|email',
            'password'    => 'required|string',
            'remember_me' => 'boolean',
        ]);    

        $credentials = request(['email', 'password']);
        $credentials['active'] = 1;
        $credentials['deleted_at'] = null;
        
        if (!Auth::attempt($credentials)) 
        {
            return response()->json(['message' => 'No Autorizado'], 401);
        }

        $user = $request->user();
        $tokenResult = $user->createToken('Token Acceso Personal');

        $token = $tokenResult->token;    

        if ($request->remember_me) 
        {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }    

        $token->save();    

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
            'user'         => $user,
        ]);
    }

    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();    

        if (!$user) {
           return response()->json(['message' => 'El token de activación es inválido'], 404);
        }    

        $user->active = true;
        $user->activation_token = '';
        $user->save();    
        return $user;
    }
    public function logout()
    {
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });
        
        return response()->json('Logged out successfully', 200);
    }
}
