<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\County;

class CountyController extends Controller
{
	public function getCountyByIdState(Request $request)
	{
		$countys = County::where('state_id', $request['idState'])->get();
		return response()->json($countys->toArray());		
	}
}