<?php 
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\State;

class StateController extends Controller
{
	public function getStates(Request $request)
	{
		$states = State::all();		
		return response()->json($states->toArray());
	}
}