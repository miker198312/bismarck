<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesPicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_pictures', function (Blueprint $table) {
            $table->id();
            $table->string('file_name');            
            $table->string('file_extension');
            $table->string('file_size');
            $table->string('description'); 
            $table->unsignedBigInteger('type_of_service_id');
            $table->foreign('type_of_service_id')->references('id')->on('type_of_services');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_pictures');
    }
}
