<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->boolean('active')->default(false);
            $table->string('activation_token');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            /*Aditional data*/
            $table->string('last_name');
            $table->string('maiden_name');
            $table->string('mobile_phone_number');      
            $table->unsignedBigInteger('county_id');
            $table->foreign('county_id')->references('id')->on('countys');
            $table->string('street');
            $table->string('external_number');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
